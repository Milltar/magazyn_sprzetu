from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect
from django.contrib import auth
from django.views.decorators.csrf import csrf_protect  
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group

@csrf_protect
def login(request):
    if request.user.username !="":
        return render(request,'index.html', {'username': request.user.username,})
    return render(request, "login.html")


def auth_view(request):   #autoryzacja
    #pobieramy dane z formularza
    username = request.POST.get('username',"")
    password = request.POST.get('password',"")
     #sprawdzamy czy dane są prawidłowe
    user = auth.authenticate(username=username, password=password)    
    if user is not None:
        auth.login(request, user)               #dodanie od sesji
        return HttpResponseRedirect('/accounts/loggedin')
    else:
        return HttpResponseRedirect('/accounts/invalid/')

@login_required
def loggedin(request):
    return render(request,'index.html', {'username': request.user.username,})

@login_required
def logout(request):
    auth.logout(request)                        #wylogowanie
    return render(request,'login.html')

def invalid_login(request):
    auth.logout(request)
    return render(request,'invalid_login.html')


