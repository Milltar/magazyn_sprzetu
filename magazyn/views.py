from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.models import User, Group
from magazyn.models import Category, Device_type, Device_cable, Storage, Device_model
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect   
from django.conf import settings 
from django.contrib import messages
from django.db.models import ProtectedError
from .function import get_massage_value
import collections
import datetime
import json
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



#======================================= Magazyny =======================================#

@login_required
def storage(request):
    #Podgląd stanu magazynów
    storage_list = Storage.objects.all().order_by("name")
    device_type = get_massage_value(request)
    return render(request,'storage.html', {"storage_list":storage_list, "device_type":device_type})

@login_required
@csrf_protect
def storage_add(request):
    #dodanie nowego magazynu
    if request.method =="POST":
        storage = Storage()
        storage.name = request.POST.get('storage_name')  
        storage.save()
        message_text = ('Dodano nowy Magazyn: "%s"' % (request.POST.get('storage_name')))
        messages.add_message(request, messages.INFO, message_text)
        return redirect("/storage/storage")
    return render(request,'storage_add.html')


@login_required
@csrf_protect
def storage_edit(request, storage_id):
    #Edycja istniejacego magazynu
    storage_to_edit = Storage.objects.get(id=storage_id)
    old_name = storage_to_edit.name
    if request.method =="POST":
        storage_to_edit.name = request.POST.get('storage_name')
        message_text = ("Zmieniono nazwę magazynu '%s' na %s" % (old_name, storage_to_edit.name))
        messages.add_message(request, messages.INFO, str(message_text))
        history_text = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"|"+request.user.first_name+" "+request.user.last_name+"|"+message_text+"||"
        storage_to_edit.history += history_text
        storage_to_edit.save()
        return redirect("/storage/storage")
    return render(request,'storage_edit.html', {"storage_name":storage_to_edit.name,"storage_id":storage_to_edit.id,})


@login_required
def storage_delete(request, storage_id):
    #Usuniecie danego magazynu
    storage_to_del = Storage.objects.get(id=storage_id)
    try:
        message_text = ("Usunięto Magazyn '%s' o id: %s" %(storage_to_del.name, storage_to_del.id))
        storage_to_del.delete()
        messages.add_message(request, messages.INFO, message_text)
    except ProtectedError as err:
        message_text = err.protected_objects
        messages.add_message(request, messages.ERROR, message_text)
    return redirect("/storage/storage")



#======================================= Kategorie =======================================#

@login_required
def category(request):
    #widok kategorii
    category_list = Category.objects.all()
    device_type = get_massage_value(request)    #pobranie odpowiedniego tekstu z modułu message
    return render(request,'category.html', {"category_list": category_list, "device_type":device_type})


@login_required
@csrf_protect
def category_add(request):
    #Dodanewanie nowej kategori
    if request.method =="POST":
        category = Category()
        category.name = request.POST.get('category_name')  
        category.save()
        message_text = ("Dodano nową kategorię: '%s'" % (request.POST.get('category_name')))
        messages.add_message(request, messages.INFO, message_text)
        return redirect("/storage/category")
    return render(request,'category_add.html')


@login_required
@csrf_protect
def category_edit(request, category_id):
    #Edycja kategorii
    category_to_edit = Category.objects.get(id=category_id)
    old_name = category_to_edit.name
    if request.method =="POST":
        category_to_edit.name = request.POST.get('category_name')
        message_text = ('Zmieniono nazwę kategorii "%s" na "%s"'% (old_name, category_to_edit.name))
        messages.add_message(request, messages.INFO, str(message_text))
        history_text = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"|"+request.user.first_name+" "+request.user.last_name+"|"+message_text+"||"
        category_to_edit.history += history_text
        category_to_edit.save()
        return redirect("/storage/category")
    return render(request,'category_edit.html', {"category_name":category_to_edit.name,"category_id":category_to_edit.id,})


@login_required
def category_delete(request, category_id):
    #Usunięcie kategorii
    category_to_del = Category.objects.get(id=category_id)
    try:
        message_text = ("Usunięto kategorię '%s' o id: %s" %(category_to_del.name, category_to_del.id))
        category_to_del.delete()
        messages.add_message(request, messages.INFO, message_text)
    except ProtectedError as err:
        #obsługa błędu związanego z usunieciem obiektu powiązanego z innym obiektem
        message_text = err.protected_objects
        messages.add_message(request, messages.ERROR, message_text)
    return redirect("/storage/category")





#======================================= Typy urzadzeń =======================================#

@login_required
def device_type(request):
    #Podgląd typu urządzeń
    device_type_list = Device_type.objects.all().order_by("category_id", "name")
    device_type = get_massage_value(request)
    category_name_list = []
    for device in device_type_list:
        category_name = device.category_id
        category_name_list.append(category_name.name)
    category_name_list = sorted(set(category_name_list))
    return render(request,'device_type.html', {"device_type_list":device_type_list,"category_name_list":category_name_list, "device_type":device_type})


@login_required
@csrf_protect
def device_type_add(request):
    #Dodawanie nowego typu urządzenia
    category_list = Category.objects.all()
    if request.method =="POST":
        category = Category.objects.get(name=request.POST.get('category_name'))
        device_type = Device_type()
        device_type.name = request.POST.get('device_type_name')
        device_type.category_id = category
        device_type.save()
        message_text = ('Dodano nowy typ urządzenia "%s" do kategorii: "%s"' % (device_type.name, device_type.category_id))
        messages.add_message(request, messages.INFO, message_text)
        return redirect("/storage/device_type")
    return render(request,'device_type_add.html', {"category_list":category_list})


@login_required
@csrf_protect
def device_type_edit(request, device_type_id):
    #Edycja typu urządzenia
    category_list = Category.objects.all()
    device_type = Device_type.objects.get(id=device_type_id)
    if request.method =="POST":
        category = Category.objects.get(name=request.POST.get('category_name'))
        device_type = Device_type.objects.get(id=device_type_id)
        old_category_name = device_type.category_id
        old_type_name = device_type.name
        device_type.name = request.POST.get('device_type_name')
        device_type.category_id = category
        message_text = ('Zmieniono kategorię: z "%s" na "%s";Zmieniono nazwę: z "%s" na "%s"' % (old_category_name, category.name, old_type_name, device_type.name))
        messages.add_message(request, messages.INFO, str(message_text))
        history_text = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"|"+request.user.first_name+" "+request.user.last_name+"|"+message_text+"||"
        device_type.history += history_text
        device_type.save()
        return redirect("/storage/device_type")
    return render(request,'device_type_edit.html', {"device_type":device_type, "category_list":category_list})


@login_required
def device_type_delete(request, device_type_id):
    #Usuniecie danego typu urządzenia
    device_type_to_del = Device_type.objects.get(id=device_type_id)
    try:
        message_text = ("Usunięto typ urządzenia '%s' o id: %s" %(device_type_to_del.name, device_type_to_del.id))
        device_type_to_del.delete()
        messages.add_message(request, messages.INFO, message_text)
    except ProtectedError as err:
        message_text = err.protected_objects
        messages.add_message(request, messages.ERROR, message_text)
    return redirect("/storage/device_type")





#======================================= Model urządzenia =======================================#

@login_required
def device_model(request):
    device_model_list = Device_model.objects.all().order_by("category_id", "device_type_id", "name")
    return render(request,'device_model.html', {"device_model_list":device_model_list,})


@login_required
@csrf_protect
def device_model_add(request):
    #Dodanie nowego modelu urządzenia
    if request.method == 'POST' and request.is_ajax():
        if request.POST.get('category_name'):
            category = Category.objects.get(name=request.POST.get('category_name'))
            device_type_list = {}
            device_type_list_temp = Device_type.objects.filter(category_id=category).order_by("name")
            for device in device_type_list_temp:
                device_type_list[device.name] = device.name
            device_type_list = collections.OrderedDict(sorted(device_type_list.items()))
            return HttpResponse(json.dumps(device_type_list), content_type="application/json")
 

    elif request.method == 'POST':
        device_category = Category.objects.get(name=request.POST.get('category_name'))
        device_type = Device_type.objects.get(name=request.POST.get('device_type_name'), category_id=device_category)
        device_model = Device_model()
        device_model.name = name=request.POST.get('name')
        device_model.category_id= device_category
        device_model.device_type_id = device_type
        device_model.save()
        return redirect("/storage/device_model")

    #Pobraneie powiązanego typu urządzenai dla pierwszej kategorii
    category_list = Category.objects.all().order_by("name")
    for category in category_list:
        device_type_list = Device_type.objects.filter(category_id=category).order_by("category_id", "name")
        category_val=category
        break

    return render(request,'device_model_add.html', {"category_list":category_list, "device_type_list":device_type_list})




#======================================= Urzadzenia/Faktury =======================================#

@login_required
def device(request):
    #Podgląd urządzeń
    device_list = Device_cable.objects.all().order_by("date", "provider","facture","name", "storage_id")
    return render(request,'device.html', {"device_list":device_list,})


@login_required
@csrf_protect
def device_add(request):
    #Dodanie nowego urządzenia
    if request.method == 'POST' and request.is_ajax():
        if request.POST.get('device_type_name') and request.POST.get('category_name'):
            category = Category.objects.get(name=request.POST.get('category_name'))
            device_type = Device_type.objects.get(name=request.POST.get('device_type_name'), category_id=category)
            device_model_list = {}
            device_model_list_temp = Device_model.objects.filter(category_id=category, device_type_id=device_type).order_by("name")
            for device in device_model_list_temp:
                device_model_list[device.name] = device.name
            device_model_list = collections.OrderedDict(sorted(device_model_list.items()))
            return HttpResponse(json.dumps(device_model_list), content_type="application/json") 
        
        #Obsługa ajaxa -> dla danej katerorii wyciaga powiązane typy urządzeń
        elif request.POST.get('category_name'):
            category = Category.objects.get(name=request.POST.get('category_name'))
            device_type_list = {}
            device_model_list = {}
            
            device_type_list_temp = Device_type.objects.filter(category_id=category).order_by("name")
            for device in device_type_list_temp:
                device_type_list[device.name] = device.name
            device_type_list = collections.OrderedDict(sorted(device_type_list.items()))

            try:
                device_type = Device_type.objects.get(category_id=category, name=list(device_type_list)[0])
                device_model_list_temp = Device_model.objects.filter(category_id=category, device_type_id=device_type).order_by("name")
                for device in device_model_list_temp:
                    device_model_list[device.name] = device.name
                device_model_list = collections.OrderedDict(sorted(device_model_list.items()))
            except:
                device_model_list={}
            args = {"device_type_list":device_type_list,"device_model_list": device_model_list}
            return HttpResponse(json.dumps(args), content_type="application/json")
 

    elif request.method == 'POST':

        device_category = Category.objects.get(name=request.POST.get('category_name'))
        device_type_name = Device_type.objects.get(name=request.POST.get('device_type_name'), category_id=device_category)
        device_model = Device_model.objects.get(name=request.POST.get('name'), category_id=device_category, device_type_id=device_type_name)
        device_storage_name = Storage.objects.get(name=request.POST.get('storage_name'))

        device = Device_cable()
        device.date = datetime.datetime.now().strftime("%Y-%m-%d")
        device.provider = request.POST.get('provider')
        device.facture = request.POST.get('facture')
        device.name = device_model
        device.serial_number = request.POST.get('serial_number')
        device.quantity = request.POST.get('quantity')
        device.guarantee = request.POST.get('guarantee')
        device.guarantee_end_date = (datetime.datetime.today() + datetime.timedelta(int(device.guarantee)*365/12)).strftime('%Y-%m-%d')
        device.used_resource = 0
        device.note = request.POST.get('note')
        device.to_used = request.POST.get('to_used')
        device.category_id = device_category
        device.device_type_id = device_type_name
        device.storage_id = device_storage_name
        device.user = request.user.first_name+" "+request.user.last_name
        device.save()

        message_text = ('Dodano nową fakture "%s"' % (device.facture))
        messages.add_message(request, messages.INFO, message_text)
        return redirect("/storage/device")


    #Pobraneie powiązanego typu urządzenai dla pierwszej kategorii
    category_list = Category.objects.all().order_by("name")
    for category in category_list:
        device_type_list = Device_type.objects.filter(category_id=category).order_by("category_id", "name")
        category_val=category
        break

    #Pobraneie powiązanego modelu urządzenai dla pierwszej kategorii i pierwszego typu
    for device_type in device_type_list:
        device_list = Device_model.objects.filter(category_id=category_val, device_type_id=device_type).order_by("category_id", "name")
        break

    storage_list = Storage.objects.all().order_by("name")
    args = {"category_list":category_list, "storage_list":storage_list, "device_type_list":device_type_list, "device_list":device_list}
    return render(request,'device_add.html', args)


@login_required
@csrf_protect
def device_edit(request, device_id):
    #Edycja urządzenia
    storage_list = Storage.objects.all().order_by("name")
    device_to_edit = Device_cable.objects.get(id=int(device_id)) 
    old_storage = device_to_edit.storage_id
    old_used_resource = device_to_edit.used_resource
    old_note = device_to_edit.note

    if request.method == 'POST':
        device_storage_name = Storage.objects.get(name=request.POST.get('storage_name'))
        device_to_edit.storage_id = device_storage_name
        device_to_edit.note = request.POST.get('note')
        
        if device_to_edit.to_used == "Tak":
            device_to_edit.used_resource = request.POST.get('used_resource')

        device_to_edit.user = request.user.first_name+" "+request.user.last_name    
        message_history = ('Zmieniono: magazyn z "%s" na "%s";Zmieniono pole wykorzystano: z "%s" na "%s"; Zmieniono: notatkę z "%s" na "%s"' \
                          % (old_storage, device_to_edit.storage_id, old_used_resource, device_to_edit.used_resource, old_note, device_to_edit.note)) 
        history_text = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"|" \
                       +request.user.first_name+" "+request.user.last_name+"| " \
                       +message_history+"||"
        device_to_edit.history += history_text
        device_to_edit.save()

        message_text = ('Zmodyfikowano urządzenie "%s" dla faktury "%s"' % (device_to_edit.name, device_to_edit.facture))
        messages.add_message(request, messages.INFO, message_text)
        return redirect("/storage/device")
    return render(request,'device_edit.html', {"storage_list":storage_list, "device_to_edit":device_to_edit})



@login_required
def history(request, type_id):
    #Wyświetlenie historii zmian danego obiektu
    (typ,typ_id) = type_id.split("-")
    if typ.strip() =="category":
        category = Category.objects.get(id=int(typ_id))
        historia = category.history
    elif typ.strip() =="storage":
        storage = Storage.objects.get(id=int(typ_id))
        historia = storage.history
    elif typ.strip() =="device":
        device = Device_cable.objects.get(id=int(typ_id))
        historia = device.history
    elif typ.strip() =="device_type":
        device_type = Device_type.objects.get(id=int(typ_id))
        historia = device_type.history

    history_list = historia.split("||")
    new_list=[]
    for history in history_list[:-1]:
        data, user, text = history.split("|")
        text = text.split(";")
        new_list.append([data, user, text])
    return render(request,'history.html', {"history_list":new_list})