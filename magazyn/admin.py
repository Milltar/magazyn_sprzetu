from django.contrib import admin
from magazyn.models import Category, Device_type, Storage, Device_cable, Device_model    

admin.site.register(Storage)
admin.site.register(Category)
admin.site.register(Device_type)
admin.site.register(Device_model)
admin.site.register(Device_cable)
