from django import template
from django.contrib.auth.models import Group 
import datetime
import calendar

register = template.Library() 

@register.filter
def get_obj_attr(obj, attr):
    #pobiera argument ze zmiennej (ze stringa) do obieku
    return getattr(obj, attr)

register.inclusion_tag('../templates/category.html')(get_obj_attr) 

@register.filter
def get_dict_attr(dict, key):
    #pobiera argument ze zmiennej (ze stringa) do obieku
    return dict[key]

register.inclusion_tag('../templates/category.html')(get_obj_attr) 


@register.filter
def modulo(num, val):
    #funkcja zwraca reszte z dzielenie modulo
    reszta = num % val
    return reszta

register.inclusion_tag('../templates/device_type.html')(modulo) 


@register.filter
def check_percent(used_resource, max_resource):
    #Oblicza procentowe wykorzystanie zasobu
    result="#fff"
    percent_result = (int(used_resource)*100)/int(max_resource)
    if int(percent_result) >= 100:
        result="#000"
    elif int(percent_result) > 70:
        result="#ffb867"
    elif int(percent_result) > 40:
        result="yellow"
    return result

register.inclusion_tag('../templates/device.html')(check_percent) 


@register.filter
def check_guarantee(guarantee_time, start_date):
    #sprawdza ile pozostało do konca gwarancji
    result="#fff"
    month = start_date.month - 1 + int(guarantee_time)
    year = start_date.year + month // 12
    month = month % 12 + 1
    day = min(start_date.day, calendar.monthrange(year,month)[1])
    end_date = datetime.date(year, month, day)
    time_to_expire = (end_date - datetime.date.today()).days / 30
    time_to_expire = int(time_to_expire)
    
    if time_to_expire <= 0:
        result="#000"
    elif time_to_expire <= 3:
        result="#ffb867"
    elif time_to_expire <= 6:
        result="yellow"

    return result

register.inclusion_tag('../templates/device.html')(check_percent) 

@register.filter
def get_guarantee(guarantee_time, start_date):
    #sprawdza ile pozostało do konca gwarancji
    result="#fff"
    month = start_date.month - 1 + int(guarantee_time)
    year = start_date.year + month // 12
    month = month % 12 + 1
    day = min(start_date.day, calendar.monthrange(year,month)[1])
    end_date = datetime.date(year, month, day)
    return end_date

register.inclusion_tag('../templates/device.html')(check_percent) 


@register.filter
def trim(value):
    #usuwa spacje na poczatka i koncu tekstu
    return str(value).strip()

register.inclusion_tag('../templates/device_type.html')(modulo) 
