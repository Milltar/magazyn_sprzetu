from django.contrib.messages import get_messages


def get_massage_value(request):
    #Konwertuje zwracany teksy z modułu messege do postaci listy wartosci
    message_value_temp = ""
    storage = get_messages(request)
    for message in storage:
        message_value_temp = str(message)
    message_value = message_value_temp.replace("<QuerySet [","").replace("<Device_type: ","").replace(">","").replace("]","").replace("<Device_cable:","").replace("<Device_model:","")
    message_value = message_value.split(",")
    return message_value