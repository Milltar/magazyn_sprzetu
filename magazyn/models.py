from django.db import models
from django.contrib.auth.models import Group

# Create your models here.


class Storage(models.Model):
    #Magazyny
    name = models.TextField(verbose_name="Nazwa magazynu")
    history = models.TextField(default="", verbose_name="Historia zmian")

    def __str__(self):
        return str(self.name)


class Category(models.Model):
    #Kategorie
    name = models.TextField(verbose_name="Nazwa kategorii")
    history = models.TextField(default="", verbose_name="Historia zmian")

    def __str__(self):
        return str(self.name)


class Device_type(models.Model):
    #Typy urządzeń
    name = models.TextField(verbose_name="Nazwa typu urzadzeń")
    history = models.TextField(default="", verbose_name="Historia zmian")
    category_id = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return str(self.name)


class Device_model(models.Model):
    #Modele urządzeń
    name = models.TextField(verbose_name="Nazwa modelu")
    category_id = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)
    device_type_id = models.ForeignKey(Device_type, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return str(self.name)


class Device_cable(models.Model):
    #Urządzenia
    date = models.DateField(verbose_name="Data dodania urządzenia")
    provider = models.TextField(verbose_name="Nazwa dostawcy urządzenia")
    facture = models.TextField(verbose_name="Nr faktury")
    name = models.ForeignKey(Device_model, on_delete=models.PROTECT, null=True)
    serial_number = models.TextField(default=None, verbose_name="Nr seryjny urządzenia")
    to_used = models.CharField(max_length=3, default="Nie", verbose_name="Czy urządzenie mozna wykorzystac")
    quantity = models.CharField(max_length=10, default="0", verbose_name="Liczba/ilośc urządzeń/kabla ")
    guarantee = models.CharField(max_length=4, default="9999", verbose_name="okres gwarancji")
    guarantee_end_date = models.DateField(verbose_name="Data koniec gwarancji")
    used_resource = models.CharField(max_length=10, default="0", verbose_name="Wykorzystanie")
    note = models.TextField(verbose_name="Dodatkowa notatka")
    user = models.TextField(verbose_name="Nazwa użytkownika dodajacego sprzet")
    category_id = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)
    device_type_id = models.ForeignKey(Device_type, on_delete=models.PROTECT, null=True)
    storage_id = models.ForeignKey(Storage, on_delete=models.PROTECT, null=True)
    history = models.TextField(verbose_name="Historia zmian")

    def __str__(self):
        return (("%s model_%s serial_%s") % (self.facture.replace(":","_").replace(" ","_").replace("__","_"), self.name, self.serial_number))
