from django.contrib import admin                                         
from django.urls import path                                             
from magazyn import views                                               

urlpatterns = [ 
    path('category', views.category, name='Zarządzenie kategoriami'),
    path('category_add/', views.category_add, name="dodanie nowej kategorii"),
    path('category_edit/<int:category_id>/', views.category_edit, name="edycja kategorii"),
    path('category_delete/<int:category_id>/', views.category_delete, name="usunięcie kategorii"),
    path('storage', views.storage, name='Zarządzenie magazynami'),
    path('storage_add/', views.storage_add, name="dodanie nowego magazynu"),
    path('storage_edit/<int:storage_id>/', views.storage_edit, name="edycja magazynu"),
    path('storage_delete/<int:storage_id>/', views.storage_delete, name="usunięcie magazynu"),
    path('device_type', views.device_type, name='Zarządzenie typami urządzeń'),
    path('device_type_add/', views.device_type_add, name='Dodanie nowego typu urządzeń'),
    path('device_type_edit/<int:device_type_id>/', views.device_type_edit, name="Edycja typu urządzenia"),
    path('device_type_delete/<int:device_type_id>/', views.device_type_delete, name="usunięcie typu urządzenia"),
    path('device', views.device, name='Zarządzenie urządzeniami'),
    path('device_add', views.device_add, name="dodanie nowego urządzenia"),
    path('device_edit/<int:device_id>/', views.device_edit, name="Edycja urządzenia"),
    path('device_model', views.device_model, name="Podgląd modeli"),
     path('device_model_add', views.device_model_add, name="Dodanie nowego modelu"),
    path('history/<str:type_id>/', views.history, name="Historia"),
    ]
