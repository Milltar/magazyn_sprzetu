from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.models import User, Group
from magazyn.models import Category, Device_type, Device_cable, Storage, Device_model
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect   
from django.conf import settings 
from django.db.models import Q
import datetime 
import collections
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

@login_required
def main_page(request):
    return render(request, "show.html")


@login_required
def guarantee_6m(request):
    #Raport urządzeń z gwarancją poniżej 6 miesięcy
    current_date=datetime.datetime.today().strftime('%Y-%m-%d')
    guarantee_end_date = (datetime.datetime.today() + datetime.timedelta(6*365/12)).strftime('%Y-%m-%d')
    device_list = Device_cable.objects.filter(guarantee_end_date__lte = guarantee_end_date, guarantee_end_date__gte = current_date)
    return render(request, "guarantee_raport.html", {"device_list":device_list})


@login_required
def guarantee_3m(request):
    #Raport urządzeń z gwarancją poniżej 3 miesięcy
    current_date=datetime.datetime.today().strftime('%Y-%m-%d')
    guarantee_end_date = (datetime.datetime.today() + datetime.timedelta(3*365/12)).strftime('%Y-%m-%d')
    device_list = Device_cable.objects.filter(guarantee_end_date__lte = guarantee_end_date, guarantee_end_date__gte = current_date)
    return render(request, "guarantee_raport.html", {"device_list":device_list})


@login_required
def guarantee0(request):
    #Raport urządzeń z brakiem gwarancji
    current_date=datetime.datetime.today().strftime('%Y-%m-%d')
    device_list = Device_cable.objects.filter(guarantee_end_date__lte = current_date)
    return render(request, "guarantee_raport.html", {"device_list":device_list})


@login_required
def sum_device(request):
    #Raport - suma urządzeń bez wykorzystania (dekodery, modemy itp)
    storage1 = Storage.objects.get(name="Sieć")
    storage2 = Storage.objects.get(name="Straty")
    device_list = Device_cable.objects.filter(Q(to_used="Nie") & ~Q(storage_id__in=[storage1, storage2]))
    device_model_name_list = [model.name for model in Device_model.objects.all()]                               #lista wszystkich modeli

    sum_device = {}
    device_model_list = []
    for device_model in device_list:
        #Pobranie wybranych parametrów dla aktywnych urzadzeń
        device_model_list.append({device_model.name.name : {
                                "name": device_model.name.name, "category": device_model.category_id.name,
                                "type": device_model.device_type_id.name, "quantity": device_model.quantity, 
                                }})

        #wyzerowanie wartosci quantity dla dla sumy
        sum_device[device_model.name.name] = {"name": device_model.name.name, "category": device_model.category_id.name,
                            "type": device_model.device_type_id.name, "quantity": 0, } 

    for model in sorted(device_model_name_list):
        for item in device_model_list:
            if item.get(model) is not None:
                sum_device[model]["quantity"] += int(item[model]["quantity"])

    return render(request, "sum_device.html", {"device_model_list":sum_device})


@login_required
def sum_device_used(request):
    #Raport - suma urządzeń które można wykorzystać -ich stan się zmniejszy (przewody, patchcordy itp)
    storage1 = Storage.objects.get(name="Sieć")
    storage2 = Storage.objects.get(name="Straty")
    device_list = Device_cable.objects.filter(Q(to_used="Tak") & ~Q(storage_id__in=[storage1, storage2]))
    device_model_name_list = [model.name for model in Device_model.objects.all()]                               #lista wszystkich modeli

    sum_device = {}
    device_model_list = []
    for device_model in device_list:
        #Pobranie wybranych parametrów dla aktywnych urzadzeń
        device_model_list.append({device_model.name.name : {
                                "name": device_model.name.name, "category": device_model.category_id.name,
                                "type": device_model.device_type_id.name, "quantity": device_model.quantity, 
                                "used_resource":device_model.used_resource,
                                }})

        #wyzerowanie wartosci quantity dla dla sumy
        sum_device[device_model.name.name] = {"name": device_model.name.name, "category": device_model.category_id.name,
                            "type": device_model.device_type_id.name, "quantity": 0, "used_resource":0} 

    for model in sorted(device_model_name_list):
        for item in device_model_list:
            if item.get(model) is not None:
                sum_device[model]["quantity"] += int(item[model]["quantity"])
                sum_device[model]["used_resource"] += int(item[model]["used_resource"])

    return render(request, "sum_device_used.html", {"device_model_list":sum_device})


@login_required
def individual_raport(request):
    category_list = Category.objects.all().order_by("name")
    storage_list = Storage.objects.all().order_by("name")

    #Obsługa ajaxa
    if request.method == 'POST' and request.is_ajax():
        if request.POST.get('device_type_name') and request.POST.get('category_name'):
            category = Category.objects.get(name=request.POST.get('category_name'))
            device_type = Device_type.objects.get(name=request.POST.get('device_type_name'), category_id=category)
            device_model_list = {}
            device_model_list_temp = Device_model.objects.filter(category_id=category, device_type_id=device_type).order_by("name")
            for device in device_model_list_temp:
                device_model_list[device.name] = device.name
            device_model_list = collections.OrderedDict(sorted(device_model_list.items()))
            return HttpResponse(json.dumps(device_model_list), content_type="application/json") 
        
        #Obsługa ajaxa -> dla danej katerorii wyciaga powiązane typy urządzeń
        elif request.POST.get('category_name'):
            category = Category.objects.get(name=request.POST.get('category_name'))
            device_type_list = {}
            device_model_list = {}
            
            device_type_list_temp = Device_type.objects.filter(category_id=category).order_by("name")
            for device in device_type_list_temp:
                device_type_list[device.name] = device.name
            device_type_list = collections.OrderedDict(sorted(device_type_list.items()))

            try:
                device_type = Device_type.objects.get(category_id=category, name=list(device_type_list)[0])
                device_model_list_temp = Device_model.objects.filter(category_id=category, device_type_id=device_type).order_by("name")
                for device in device_model_list_temp:
                    device_model_list[device.name] = device.name
                device_model_list = collections.OrderedDict(sorted(device_model_list.items()))
            except:
                device_model_list={}
            args = {"device_type_list":device_type_list,"device_model_list": device_model_list}
            
            return HttpResponse(json.dumps(args), content_type="application/json")
    
    #obsługa formularza
    if request.method == 'POST':

        device_args = {}
        #pobranie danych z formularza
        if request.POST.get('storage_name') != "Wszystkie" and request.POST.get('storage_name') != "":
            device_args["storage_id"] = Storage.objects.get(name=request.POST.get('storage_name'))

        if request.POST.get('category_name') != "Wszystkie" and request.POST.get('category_name') != "":
            device_args["category_id"] = Category.objects.get(name=request.POST.get('category_name')) 

        if request.POST.get('device_type_name') != "Wszystkie" and request.POST.get('device_type_name') != "":
            device_args["device_type_id"] = Device_type.objects.get(name=request.POST.get('device_type_name'), category_id=device_args["category_id"])

        if request.POST.get('name') != "Wszystkie" and request.POST.get('name') != "":
            device_args["name"] = Device_model.objects.get(name=request.POST.get('name'), category_id=device_args["category_id"], device_type_id=device_args["device_type_id"])

        device_list = Device_cable.objects.filter(**device_args) if device_args else Device_cable.objects.all()
  
        device_model_name_list = [model.name for model in Device_model.objects.all()]
        sum_device = {}
        device_model_list = []
        for device_model in device_list:
            #Pobranie wybranych parametrów dla aktywnych urzadzeń
            device_model_list.append({device_model.name.name : {
                                    "name": device_model.name.name, "category": device_model.category_id.name,
                                    "type": device_model.device_type_id.name, "quantity": device_model.quantity, 
                                    "used_resource":device_model.used_resource,
                                    }})

            #wyzerowanie wartosci quantity dla dla sumy
            sum_device[device_model.name.name] = {"name": device_model.name.name, "category": device_model.category_id.name,
                                "type": device_model.device_type_id.name, "quantity": 0, "used_resource":0} 

        for model in sorted(device_model_name_list):
            for item in device_model_list:
                if item.get(model) is not None:
                    sum_device[model]["quantity"] += int(item[model]["quantity"])
                    sum_device[model]["used_resource"] += int(item[model]["used_resource"])
        
        magazyn =  device_args["storage_id"] if device_args.get("storage_id") is not None else "Wszystkie"

        args ={"category_list":category_list, "storage_list":storage_list,"device_list":device_list,"device_model_list":sum_device, "magazyn_name":magazyn}

        return render(request, "individual_raport.html", args)

    return render(request, "individual_raport.html", {"category_list":category_list, "storage_list":storage_list,})