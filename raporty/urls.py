from django.contrib import admin                                          
from django.urls import path                                             
from raporty import views                                                 

urlpatterns = [ 
    path('show', views.main_page, name='Zbiór raprtów'),
    path('guarantee_6m', views.guarantee_6m, name='Raport urądzeń z gwarancją mniejszą niż 6 miesięcy'),
    path('guarantee_3m', views.guarantee_3m, name='Raport urądzeń z gwarancją mniejszą niż 3 miesięce'),
    path('guarantee0', views.guarantee0, name='Raport urządzeń po gwarancji'),
    path('sum_device', views.sum_device, name='Raport stanu/ilości urządzeń'),
    path('sum_device_used', views.sum_device_used, name='Raport stosunku stanu/ilości urządzeń do ich wykorzystania'),
    path('individual_raport', views.individual_raport, name='Raport w oparciu o własne dane'),
    ]
